## Mount Sharp SLC

Mount Sharp SLC is a Project Tango prototype

 * WIP:
    * Utilizing depth data for obstacle avoidance
    * Adding Area-Learning functionality
    * Using the best-fit plane function from the AR sample app for map visualization

 * Notes:
    * Depends on the TangoUtils code from the [Project Tango Java samples](https://github.com/googlesamples/tango-examples-java)