/*
 * Copyright 2014 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.joshfinken.sharpslc;

import com.google.atap.tango.ux.TangoUx;
import com.google.atap.tango.ux.TangoUx.StartParams;
import com.google.atap.tango.ux.TangoUxLayout;
import com.google.atap.tango.ux.UxExceptionEvent;
import com.google.atap.tango.ux.UxExceptionEventListener;
import com.google.atap.tangoservice.Tango;
import com.google.atap.tangoservice.TangoCameraIntrinsics;
import com.google.atap.tangoservice.Tango.OnTangoUpdateListener;
import com.google.atap.tangoservice.TangoConfig;
import com.google.atap.tangoservice.TangoCoordinateFramePair;
import com.google.atap.tangoservice.TangoErrorException;
import com.google.atap.tangoservice.TangoEvent;
import com.google.atap.tangoservice.TangoException;
import com.google.atap.tangoservice.TangoOutOfDateException;
import com.google.atap.tangoservice.TangoPoseData;
import com.google.atap.tangoservice.TangoXyzIjData;
import com.google.atap.tangoservice.TangoPointCloudData;
import com.google.atap.tango.reconstruction.TangoFloorplanLevel;
import com.google.atap.tango.reconstruction.TangoPolygon;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Surface;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.hardware.display.DisplayManager;
//import android.hardware.Camera;
import android.view.Display;
import android.opengl.Matrix;

import com.projecttango.tangosupport.TangoPointCloudManager;
import com.projecttango.tangosupport.TangoSupport;
import com.projecttango.tangosupport.TangoSupport.IntersectionPointPlaneModelPair;
//import com.joshfinken.sharpslc.rajawali.DeviceExtrinsics;
//import com.joshfinken.sharpslc.rajawali.ScenePoseCalculator;
//import com.joshfinken.sharpslc.TangoPoseUtilities;

import org.rajawali3d.scene.ASceneFrameCallback;
import org.rajawali3d.surface.RajawaliSurfaceView;

import android.os.Handler;
import java.lang.Override;
import java.nio.FloatBuffer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Main Activity class for the Point Cloud Sample. Handles the connection to the {@link Tango}
 * service and propagation of Tango point data to OpenGL and Layout views. OpenGL rendering logic is
 * delegated to the {@link SharpSLCRajawaliRenderer} class.
 */
public class SharpSLCActivity extends Activity implements OnClickListener {

    private static final String TAG = SharpSLCActivity.class.getSimpleName();
    private static final int SECS_TO_MILLISECS = 1000;

    public static final TangoCoordinateFramePair FRAME_PAIR = new TangoCoordinateFramePair(
            TangoPoseData.COORDINATE_FRAME_START_OF_SERVICE,
            TangoPoseData.COORDINATE_FRAME_DEVICE);

    // Configure the Tango coordinate frame pair
    private static final ArrayList<TangoCoordinateFramePair> FRAME_PAIRS =
            new ArrayList<TangoCoordinateFramePair>();
    {
        FRAME_PAIRS.add(new TangoCoordinateFramePair(
                TangoPoseData.COORDINATE_FRAME_START_OF_SERVICE,
                TangoPoseData.COORDINATE_FRAME_DEVICE));
    }

    private Tango mTango;
    private TangoConfig mConfig;

    private SharpSLCRajawaliRenderer mRenderer;
    private RajawaliSurfaceView glView; //= (RajawaliSurfaceView) findViewById(R.id.gl_surface_view);
    private boolean mIsConnected = false;

    private TextView mDeltaTextView;
    //private TextView mPoseCountTextView;
    private TextView mPoseTextView;
    private TextView mQuatTextView;
    private TextView mPoseStatusTextView;
    private TextView mTangoEventTextView;
    private TextView mPointCountTextView;
    //private TextView mTangoServiceVersionTextView;
    private TextView mApplicationVersionTextView;
    private TextView mAverageZTextView;
    private TextView mFrequencyTextView;

    //-------------------------------------------------------------------------
    // Plane data and thresholds for obstacle avoidance
    //-------------------------------------------------------------------------
    private final String CENTER = "center";
    private final String LEFT = "left";
    private final String RIGHT = "right";
    private final double PLANE_DIST_THRESH = 0.8;
    //private final double LEFT_PLANE_DIST_THRESH = 1.0;
    //private final double RIGHT_PLANE_DIST_THRESH = 1.0;
    private TextView mDistToPlaneLeftTextView;
    private TextView mDistToPlaneCenterTextView;
    private TextView mDistToPlaneRightTextView;
    private TextView mNumFloorplanPolysTextView;
    private String mDistToPlaneLeft;
    private String mDistToPlaneRight;
    private String mDistToPlaneCenter;
    private HashMap<String, SharpSLCPlane> planes;

    //-------------------------------------------------------------------------
    // Floor Planner
    //-------------------------------------------------------------------------
    private TangoFloorplanner mTangoFloorplanner;
    private String mNumFloorplanPolys;

    private Button mFirstPersonButton;
    private Button mThirdPersonButton;
    private Button mTopDownButton;
    private Button mGobotButton;

    //private int mCount;
    private int mPreviousPoseStatus = TangoPoseData.POSE_INVALID;
    private double mPosePreviousTimeStamp;
    //private double mXyIjPreviousTimeStamp;
    private double mPointCloudPreviousTimeStamp;
    //private boolean mIsTangoServiceConnected;
    private TangoPoseData mPose;
    private TangoPointCloudManager mPointCloudManager;
    private TangoUx mTangoUx;

    private double[] mCurrentPlane;

    private static final DecimalFormat FORMAT_THREE_DECIMAL = new DecimalFormat("0.000");
    private static final double UPDATE_INTERVAL_MS = 100.0;

    private double mPoseTimeToNextUpdate = UPDATE_INTERVAL_MS;
    private double mPointCloudTimeToNextUpdate = UPDATE_INTERVAL_MS;

    // Timer: sample and add planes
    private Handler planeHandler;
    private Handler robotHandler;
    private Runnable robotRunnable;
    private final int robotSampleMs = 3500;
    private boolean ROBOT_CONTROL_LIVE = false;
    private boolean FEW_DEPTH_POINTS_TANGOUX = false;

    //private int mDepthCameraToDisplayRotation = 0;
    private int mDisplayRotation = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jpoint_cloud);
        setTitle(R.string.app_name);
        setupTextViewsAndButtons();

        mRenderer = setupGLViewAndRenderer();

        mPointCloudManager = new TangoPointCloudManager();
        mTangoUx = setupTangoUxAndLayout();
        setupRenderer();

        DisplayManager displayManager = (DisplayManager) getSystemService(DISPLAY_SERVICE);
        if (displayManager != null) {
            displayManager.registerDisplayListener(new DisplayManager.DisplayListener() {
                @Override
                public void onDisplayAdded(int displayId) {

                }

                @Override
                public void onDisplayChanged(int displayId) {
                    synchronized (this) {
                        setAndroidOrientation();
                    }
                }

                @Override
                public void onDisplayRemoved(int displayId) {}
            }, null);
        }

        //---------------------------------------------------------------------
        // Timer thread to sample the 2D scene and add best fit planes
        //---------------------------------------------------------------------
        final int sampleMs = 100;
        planeHandler = new Handler();
        planeHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sampleBestFitPlanes();
                planeHandler.postDelayed(this, sampleMs);
            }
        }, sampleMs);
        //---------------------------------------------------------------------
        // Setup timer thread for autonomous robot control
        //---------------------------------------------------------------------
        robotHandler = new Handler();
        robotRunnable = new Runnable() {
            @Override
            public void run() {
                controlRobot();
                robotHandler.postDelayed(this, robotSampleMs);
            }
        };
        buildPlanes();
    }
    /**
     * Sets Rajawali surface view and its renderer. This is ideally called only once in onCreate.
     */
    public void setupRenderer() {
        glView.setEGLContextClientVersion(2);
        mRenderer.getCurrentScene().registerFrameCallback(new ASceneFrameCallback() {
            @Override
            public void onPreFrame(long sceneTime, double deltaTime) {
                // NOTE: This will be executed on each cycle before rendering, called from the
                // OpenGL rendering thread

                // Prevent concurrent access from a service disconnect through the onPause event.
                synchronized (SharpSLCActivity.this) {
                    // Don't execute any tango API actions if we're not connected to the service.
                    if (!mIsConnected) {
                        return;
                    }

                    // Update point cloud data.
                    TangoPointCloudData pointCloud = mPointCloudManager.getLatestPointCloud();
                    if (pointCloud != null) {
                        // Calculate the camera color pose at the camera frame update time in
                        // OpenGL engine.
                        TangoSupport.TangoMatrixTransformData transform =
                                TangoSupport.getMatrixTransformAtTime(pointCloud.timestamp,
                                        TangoPoseData.COORDINATE_FRAME_START_OF_SERVICE,
                                        TangoPoseData.COORDINATE_FRAME_CAMERA_DEPTH,
                                        TangoSupport.TANGO_SUPPORT_ENGINE_OPENGL,
                                        TangoSupport.TANGO_SUPPORT_ENGINE_TANGO,
                                        Surface.ROTATION_0);
                        if (transform.statusCode == TangoPoseData.POSE_VALID) {
                            mRenderer.updatePointCloud(pointCloud, transform.matrix);
                        }
                    }

                    // Update current camera pose.
                    try {
                        // Calculate the device pose. This transform is used to display
                        // frustum in third and top down view, and used to render camera pose in
                        // first person view.
                        TangoPoseData lastFramePose = TangoSupport.getPoseAtTime(0,
                                TangoPoseData.COORDINATE_FRAME_START_OF_SERVICE,
                                TangoPoseData.COORDINATE_FRAME_DEVICE,
                                TangoSupport.TANGO_SUPPORT_ENGINE_OPENGL,
                                TangoSupport.TANGO_SUPPORT_ENGINE_OPENGL,
                                mDisplayRotation);
                        if (lastFramePose.statusCode == TangoPoseData.POSE_VALID) {
                            mRenderer.updateCameraPose(lastFramePose);
                        }
                    } catch (TangoErrorException e) {
                        Log.e(TAG, "Could not get valid transform");
                    }
                }
            }

            @Override
            public boolean callPreFrame() {
                return true;
            }

            @Override
            public void onPreDraw(long sceneTime, double deltaTime) {

            }

            @Override
            public void onPostFrame(long sceneTime, double deltaTime) {

            }
        });
        glView.setSurfaceRenderer(mRenderer);
    }
    // UV: 0,0 is upper-left?
    private void buildPlanes() {
        planes = new HashMap<String, SharpSLCPlane>();
        planes.put(LEFT, new SharpSLCPlane(LEFT, 0.0f, 0.75f));
        planes.put(CENTER, new SharpSLCPlane(CENTER, 0.5f, 0.75f));
        planes.put(RIGHT, new SharpSLCPlane(RIGHT, 0.99f, 0.75f));
    }
    @Override
    protected void onPause() {
        super.onPause();

        // Synchronize against disconnecting while the service is being used in the OpenGL
        // thread or in the UI thread.
        // NOTE: DO NOT lock against this same object in the Tango callback thread.
        // Tango.disconnect will block here until all Tango callback calls are finished.
        // If you lock against this object in a Tango callback thread it will cause a deadlock.
        synchronized (this) {
            try {
                mTangoUx.stop();
                mTangoFloorplanner.stopFloorplanning();
                mTango.disconnect();
                mRenderer.getCurrentScene().clearFrameCallbacks();
                mTangoFloorplanner.resetFloorplan();
                mTangoFloorplanner.release();
                mIsConnected = false;
            } catch (TangoErrorException e) {
                Toast.makeText(getApplicationContext(), R.string.TangoError, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        setAndroidOrientation();

        StartParams params = new StartParams();
        // Shows/Disables the "Hold Still" screen on startup.
        params.showConnectionScreen = true;
        mTangoUx.start(params);
        // Initialize Tango Service as a normal Android Service, since we call mTango.disconnect()
        // in onPause, this will unbind Tango Service, so every time when onResume gets called, we
        // should create a new Tango object.
        mTango = new Tango(SharpSLCActivity.this, new Runnable() {
            // Pass in a Runnable to be called from UI thread when Tango is ready, this Runnable
            // will be running on a new thread.
            // When Tango is ready, we can call Tango functions safely here only when there is no UI
            // thread changes involved.
            @Override
            public void run() {
                // Synchronize against disconnecting while the service is being used in the OpenGL
                // thread or in the UI thread.
                synchronized (SharpSLCActivity.this) {
                    try {
                        TangoSupport.initialize();
                        mConfig = setupTangoConfig(mTango);
                        mTango.connect(mConfig);
                        setTangoListeners();
                        //mPointCloudManager = new TangoPointCloudManager();
                        setupExtrinsics();
                        //setIntrinsics();
                        mIsConnected = true;
                    } catch (TangoOutOfDateException e) {
                        if (mTangoUx != null) {
                            mTangoUx.showTangoOutOfDate();
                        }
                    } catch (TangoErrorException e) {
                        Toast.makeText(getApplicationContext(), R.string.TangoError, Toast.LENGTH_SHORT)
                                .show();
                    }
                }
            }
        });
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.first_person_button:
            mRenderer.setFirstPersonView();
            break;
        case R.id.third_person_button:
            mRenderer.setThirdPersonView();
            break;
        case R.id.top_down_button:
            mRenderer.setTopDownView();
            break;
        case R.id.robot_control_button:
            toggleRobotControl();
            break;
        default:
            Log.w(TAG, "Unrecognized button click.");
        }
    }
    private void toggleRobotControl(){
        if (ROBOT_CONTROL_LIVE)
            robotHandler.removeCallbacks(robotRunnable);
        else
            robotHandler.postDelayed(robotRunnable, robotSampleMs);
        ROBOT_CONTROL_LIVE = !ROBOT_CONTROL_LIVE;
    }
    private void controlRobot() {
        /*
            Workflow (synchronous):
            - sample planes
            - make decision
            - GET request to robot control service

            Very simple obstacle avoidance heuristic:
            - given the windowed-average distances, pick the furthest away plane
            - if that distance is less than THRESH, backup
            - else travel in the direction of the plane that is furthest away.

            Questions:
            - duration of movement relative to how often we tell the robot to go?
         */
        // See mUxExceptionListener:  too few depth points, the 'Nothing Detected' UX error.
        if (FEW_DEPTH_POINTS_TANGOUX)
            return;
        double cAvg = planes.get(CENTER).getAvgDist();
        double lAvg = planes.get(LEFT).getAvgDist();
        double rAvg = planes.get(RIGHT).getAvgDist();

        // Not enough samples
        if (cAvg <= 0 || lAvg <= 0 || rAvg <=0)
            return;


        // If left or right dominates, turn that direction.  Otherwise the
        // percent-difference is minimal or in fact the distance to the center plane
        // is greater, go straight
        double LEFT_RIGHT_PERC_DIFF = -0.25;
        double clPercDiff = (cAvg - lAvg)/((cAvg + lAvg) * 0.5);
        double crPercDiff = (cAvg - rAvg)/((cAvg + rAvg) * 0.5);

        // reverse then turn left or right
        if (cAvg <= PLANE_DIST_THRESH && lAvg <= PLANE_DIST_THRESH && rAvg <= PLANE_DIST_THRESH) {
            SharpSLCRobotService.getInstance(this).ControlRequest(SharpSLCRobotService.BACKWARD, 2);
            if (lAvg >= rAvg)
                SharpSLCRobotService.getInstance(this).ControlRequest(SharpSLCRobotService.LEFT, 1);
            else
                SharpSLCRobotService.getInstance(this).ControlRequest(SharpSLCRobotService.RIGHT, 1);
        }
        else if (clPercDiff <= LEFT_RIGHT_PERC_DIFF)
            SharpSLCRobotService.getInstance(this).ControlRequest(SharpSLCRobotService.LEFT, 1);
        else if (crPercDiff <= LEFT_RIGHT_PERC_DIFF)
            SharpSLCRobotService.getInstance(this).ControlRequest(SharpSLCRobotService.RIGHT, 1);
        else
            SharpSLCRobotService.getInstance(this).ControlRequest(SharpSLCRobotService.FORWARD, 2);

        // Set this set to previous.  Basically a check to assert we're using "fresh" data.
        planes.get(CENTER).setPrevAvgDist(cAvg);
        planes.get(LEFT).setPrevAvgDist(lAvg);
        planes.get(RIGHT).setPrevAvgDist(rAvg);

        /*
        if (max <= PLANE_DIST_THRESH)
            SharpSLCRobotService.getInstance(this).ControlRequest(SharpSLCRobotService.BACKWARD, 2);
        // else just travel towards the direction that is furthest away
        else if (max == cAvg)
            SharpSLCRobotService.getInstance(this).ControlRequest(SharpSLCRobotService.FORWARD, 2);
        else if (max == lAvg)
            SharpSLCRobotService.getInstance(this).ControlRequest(SharpSLCRobotService.LEFT, 1);
        else
            SharpSLCRobotService.getInstance(this).ControlRequest(SharpSLCRobotService.RIGHT, 1);
        */

        /*
        if (cAvg >= CENTER_PLANE_DIST_THRESH) {
            SharpSLCRobotService.getInstance(this).ControlRequest(SharpSLCRobotService.FORWARD, 2);
        } else if (lAvg <= LEFT_PLANE_DIST_THRESH && cAvg <= CENTER_PLANE_DIST_THRESH) {
            SharpSLCRobotService.getInstance(this).ControlRequest(SharpSLCRobotService.RIGHT, 1);
        } else if (rAvg <= RIGHT_PLANE_DIST_THRESH && cAvg <= CENTER_PLANE_DIST_THRESH) {
            SharpSLCRobotService.getInstance(this).ControlRequest(SharpSLCRobotService.LEFT, 1);
        } else if (lAvg <= LEFT_PLANE_DIST_THRESH && rAvg <= RIGHT_PLANE_DIST_THRESH && cAvg <= CENTER_PLANE_DIST_THRESH) {
            SharpSLCRobotService.getInstance(this).ControlRequest(SharpSLCRobotService.BACKWARD, 1);
        }
        */
    }
    private void sampleBestFitPlanes() {
        // Previously this equaled the click location in u,v (0;1) coordinates.
        //float u = event.getX() / mRenderer.getViewportWidth();
        //float v = event.getY() / mRenderer.getViewportHeight();
        // Lower-Left is 0,0 orientation...

        for (HashMap.Entry<String, SharpSLCPlane> entry : planes.entrySet()) {
            SharpSLCPlane plane = entry.getValue();
            try {
                // Fit a plane on the U,V point using the latest point cloud data
                // mRenderer.getTimestamp() has to do with when the camera set the frame as texture
                float[] planeFitPose = doFitPlane(plane.getU(), plane.getV(), mPosePreviousTimeStamp);
                if (mCurrentPlane == null)
                    return;
                plane.setPlaneEquation(mCurrentPlane[0], mCurrentPlane[1], mCurrentPlane[2], mCurrentPlane[3]);

                // for updating the UI
                if (plane.getDirection().equals(LEFT))
                    mDistToPlaneLeft = plane.getUi();
                else if (plane.getDirection().equals(CENTER))
                    mDistToPlaneCenter = plane.getUi();
                else
                    mDistToPlaneRight = plane.getUi();

                if (planeFitPose != null) {
                    // Update the position of the rendered cube to the pose of the detected plane
                    // This update is made thread safe by the renderer
                    //mRenderer.addObjectPose(planeFitPose, mCurrentPlane);
                }

            } catch (TangoException t) {
                /*
                Toast.makeText(getApplicationContext(),
                        R.string.failed_measurement,
                        Toast.LENGTH_SHORT).show();
                Log.e(TAG, getString(R.string.failed_measurement), t);
                */
            } catch (SecurityException t) {
                Toast.makeText(getApplicationContext(),
                        R.string.failed_permissions,
                        Toast.LENGTH_SHORT).show();
                Log.e(TAG, getString(R.string.failed_permissions), t);
            }
        }
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mRenderer.onTouchEvent(event);
        return true;
    }

    private void sendFloorplan(List<TangoPolygon> polygons) {

        // TODO: throttle this to 1 per sec?
        // Make a shallow copy in case mPolygons is reset while rendering.
        List<TangoPolygon> drawPolygons = polygons;
        SharpSLCRobotService.getInstance(this).PutFloorplanRequest(drawPolygons);
    }
    private void setTangoListeners() {

        // Floor Planner
        mTangoFloorplanner = new TangoFloorplanner(new TangoFloorplanner
                .OnFloorplanAvailableListener() {
            @Override
            public void onFloorplanAvailable(List<TangoPolygon> polygons,
                                             List<TangoFloorplanLevel> levels) {

                // Send floor plan to Service
                sendFloorplan(polygons);

                // Quick UI
                mNumFloorplanPolys = String.format(java.util.Locale.US, "Current Floorplan polygons: %d", polygons.size());

                // Nice to have
                //updateFloorAndCeiling(levels);
                //calculateAndUpdateArea(polygons);
            }
        });
        // Set camera intrinsics to TangoFloorplanner.
        mTangoFloorplanner.setDepthCameraCalibration(mTango.getCameraIntrinsics
                (TangoCameraIntrinsics.TANGO_CAMERA_DEPTH));

        mTangoFloorplanner.startFloorplanning();

        // Listen for new Tango data
        mTango.connectListener(FRAME_PAIRS, new OnTangoUpdateListener() {
            @Override
            public void onPoseAvailable(final TangoPoseData pose) {
                // Passing in the pose data to UX library produce exceptions.
                if (mTangoUx != null) {
                    mTangoUx.updatePoseStatus(pose.statusCode);
                }

                // Update our copy of the latest pose
                // Synchronize against concurrent use in the render loop.
                synchronized (this) {
                    mPose = pose;
                }

                // Calculate the delta time from previous pose.
                final double deltaTime = (pose.timestamp - mPosePreviousTimeStamp)
                        * SECS_TO_MILLISECS;
                mPosePreviousTimeStamp = pose.timestamp;
                /*
                if (mPreviousPoseStatus != pose.statusCode) {
                    mCount = 0;
                }
                mCount++;
                */
                mPreviousPoseStatus = pose.statusCode;
                mPoseTimeToNextUpdate -= deltaTime;

                if (mPoseTimeToNextUpdate < 0.0) {
                    mPoseTimeToNextUpdate = UPDATE_INTERVAL_MS;

                    final String translationString
                            = TangoPoseUtilities.getTranslationString(mPose, FORMAT_THREE_DECIMAL);
                    final String quaternionString
                            = TangoPoseUtilities.getQuaternionString(mPose, FORMAT_THREE_DECIMAL);
                    final String status = TangoPoseUtilities.getStatusString(mPose);
                    //final String countString = Integer.toString(mCount);
                    //final Vector3 ypr = ScenePoseCalculator.tangoPoseToYPR(pose);
                    //Log.i(TAG, "YPR: "+ypr.toString());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mPoseTextView.setText(translationString);
                            mQuatTextView.setText(quaternionString);
                            //mPoseCountTextView.setText(countString);
                            mDeltaTextView.setText(FORMAT_THREE_DECIMAL.format(deltaTime));
                            mPoseStatusTextView.setText(status);
                        }
                    });
                }
            }

            @Override
            public void onXyzIjAvailable(TangoXyzIjData xyzIj) {
                // We are not using onXyzIjAvailable for this app.
            }

            @Override
            public void onTangoEvent(final TangoEvent event) {
                if (mTangoUx != null) {
                    mTangoUx.updateTangoEvent(event);
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mTangoEventTextView.setText(event.eventKey + ": " + event.eventValue);
                    }
                });
            }

            @Override
            public void onPointCloudAvailable(TangoPointCloudData pointCloud) {
                if (mTangoUx != null) {
                    mTangoUx.updateXyzCount(pointCloud.numPoints);
                }

                // Also give the cloud to the Floor Planner
                mTangoFloorplanner.onPointCloudAvailable(pointCloud);

                mPointCloudManager.updatePointCloud(pointCloud);
                FEW_DEPTH_POINTS_TANGOUX = false;

                final double currentTimeStamp = pointCloud.timestamp;
                final double pointCloudFrameDelta =
                        (currentTimeStamp - mPointCloudPreviousTimeStamp) * SECS_TO_MILLISECS;
                mPointCloudPreviousTimeStamp = currentTimeStamp;
                final double averageDepth = getAveragedDepth(pointCloud.points,
                        pointCloud.numPoints);

                mPointCloudTimeToNextUpdate -= pointCloudFrameDelta;

                if (mPointCloudTimeToNextUpdate < 0.0) {
                    mPointCloudTimeToNextUpdate = UPDATE_INTERVAL_MS;
                    final String pointCountString = Integer.toString(pointCloud.numPoints);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mPointCountTextView.setText(pointCountString);
                            mFrequencyTextView.setText(FORMAT_THREE_DECIMAL.format(pointCloudFrameDelta));
                            mAverageZTextView.setText(FORMAT_THREE_DECIMAL.format(averageDepth));
                            mDistToPlaneLeftTextView.setText(mDistToPlaneLeft);
                            mDistToPlaneCenterTextView.setText(mDistToPlaneCenter);
                            mDistToPlaneRightTextView.setText(mDistToPlaneRight);
                            mNumFloorplanPolysTextView.setText(mNumFloorplanPolys);
                        }
                    });
                }
            }

            @Override
            public void onFrameAvailable(int cameraId) {
                // We are not using onFrameAvailable for this application.
            }
        });
    }

    /**
     * Calculates and stores the fixed transformations between the device and
     * the various sensors to be used later for transformations between frames.
     */
    private void setupExtrinsics() {
        // Create camera to IMU transform.
        TangoCoordinateFramePair framePair = new TangoCoordinateFramePair();
        framePair.baseFrame = TangoPoseData.COORDINATE_FRAME_IMU;
        framePair.targetFrame = TangoPoseData.COORDINATE_FRAME_CAMERA_COLOR;
        TangoPoseData imuTColorCameraPose = mTango.getPoseAtTime(0.0,framePair);

        // Create depth camera to IMU transform.
        framePair.targetFrame = TangoPoseData.COORDINATE_FRAME_CAMERA_DEPTH;
        TangoPoseData imuTDepthCameraPose = mTango.getPoseAtTime(0.0,framePair);

        // Create device to IMU transform.
        framePair.targetFrame = TangoPoseData.COORDINATE_FRAME_DEVICE;
        TangoPoseData imuTDevicePose = mTango.getPoseAtTime(0.0, framePair);

        mRenderer.setupExtrinsics(imuTDevicePose, imuTColorCameraPose, imuTDepthCameraPose);
    }

   /*
   * This is an advanced way of using UX exceptions. In most cases developers can just use the in
   * built exception notifications using the Ux Exception layout. In case a developer doesn't want
   * to use the default Ux Exception notifications, he can set the UxException listener as shown
   * below.
   * In this example we are just logging all the ux exceptions to logcat, but in a real app,
   * developers should use these exceptions to contextually notify the user and help direct the
   * user in using the device in a way Tango service expects it.
   */
    private UxExceptionEventListener mUxExceptionListener = new UxExceptionEventListener() {

        @Override
        public void onUxExceptionEvent(UxExceptionEvent uxExceptionEvent) {
            if(uxExceptionEvent.getType() == UxExceptionEvent.TYPE_LYING_ON_SURFACE){
                Log.i(TAG, "Device lying on surface ");
            }
            if(uxExceptionEvent.getType() == UxExceptionEvent.TYPE_FEW_DEPTH_POINTS){
                Log.i(TAG, "Very few depth points in mPoint cloud " );
                FEW_DEPTH_POINTS_TANGOUX = true;
            }
            if(uxExceptionEvent.getType() == UxExceptionEvent.TYPE_FEW_FEATURES){
                Log.i(TAG, "Invalid poses in MotionTracking ");
            }
            if(uxExceptionEvent.getType() == UxExceptionEvent.TYPE_INCOMPATIBLE_VM){
                Log.i(TAG, "Device not running on ART");
            }
            if(uxExceptionEvent.getType() == UxExceptionEvent.TYPE_MOTION_TRACK_INVALID){
                Log.i(TAG, "Invalid poses in MotionTracking ");
            }
            if(uxExceptionEvent.getType() == UxExceptionEvent.TYPE_MOVING_TOO_FAST){
                Log.i(TAG, "Invalid poses in MotionTracking ");
            }
            if(uxExceptionEvent.getType() == UxExceptionEvent.TYPE_OVER_EXPOSED){
                Log.i(TAG, "Camera Over Exposed");
            }
            if(uxExceptionEvent.getType() == UxExceptionEvent.TYPE_TANGO_SERVICE_NOT_RESPONDING){
                Log.i(TAG, "TangoService is not responding ");
            }
            if(uxExceptionEvent.getType() == UxExceptionEvent.TYPE_UNDER_EXPOSED){
                Log.i(TAG, "Camera Under Exposed " );
            }

        }
    };

    /**
     * Sets up the tango configuration object. Make sure mTango object is initialized before
     * making this call.
     */
    private TangoConfig setupTangoConfig(Tango tango) {
        // Use the default configuration plus add depth sensing.
        TangoConfig config = tango.getConfig(TangoConfig.CONFIG_TYPE_DEFAULT);
        config.putBoolean(TangoConfig.KEY_BOOLEAN_DEPTH, true);
        config.putInt(TangoConfig.KEY_INT_DEPTH_MODE, TangoConfig.TANGO_DEPTH_MODE_POINT_CLOUD);
        // Drift correction allows motion tracking to recover after it loses tracking.
        // The drift-corrected pose is available through the frame pair with
        // base frame AREA_DESCRIPTION and target frame DEVICE.
        // JF NOTE: plane-fitting requires this boolean to be true.
        config.putBoolean(TangoConfig.KEY_BOOLEAN_DRIFT_CORRECTION, true);
        return config;
    }

    /**
     * Sets Text views to display statistics of Poses being received. This also sets the buttons
     * used in the UI.
     */
    private void setupTextViewsAndButtons(){
        mPoseTextView = (TextView) findViewById(R.id.pose);
        mQuatTextView = (TextView) findViewById(R.id.quat);
        //mPoseCountTextView = (TextView) findViewById(R.id.posecount);
        mDeltaTextView = (TextView) findViewById(R.id.deltatime);
        mTangoEventTextView = (TextView) findViewById(R.id.tangoevent);
        mPoseStatusTextView = (TextView) findViewById(R.id.status);
        mPointCountTextView = (TextView) findViewById(R.id.pointCount);
        //mTangoServiceVersionTextView = (TextView) findViewById(R.id.version);
        mApplicationVersionTextView = (TextView) findViewById(R.id.appversion);
        mAverageZTextView = (TextView) findViewById(R.id.averageZ);
        mFrequencyTextView = (TextView) findViewById(R.id.frameDelta);
        mDistToPlaneLeftTextView = (TextView) findViewById(R.id.distToPlaneLeft);
        mDistToPlaneCenterTextView = (TextView) findViewById(R.id.distToPlaneCenter);
        mDistToPlaneRightTextView = (TextView) findViewById(R.id.distToPlaneRight);
        mNumFloorplanPolysTextView = (TextView) findViewById(R.id.floorplanPolys);

        mFirstPersonButton = (Button) findViewById(R.id.first_person_button);
        mFirstPersonButton.setOnClickListener(this);
        mThirdPersonButton = (Button) findViewById(R.id.third_person_button);
        mThirdPersonButton.setOnClickListener(this);
        mTopDownButton = (Button) findViewById(R.id.top_down_button);
        mTopDownButton.setOnClickListener(this);
        mGobotButton = (Button) findViewById(R.id.robot_control_button);
        mGobotButton.setOnClickListener(this);

        PackageInfo packageInfo;
        try {
            packageInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
            mApplicationVersionTextView.setText(packageInfo.versionName);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }

        // Display the version of Tango Service
        //String serviceVersion = config.getString("tango_service_library_version");
        //mTangoServiceVersionTextView.setText(serviceVersion);
    }

    /**
     * Sets Rajawalisurface view and its renderer. This is ideally called only once in onCreate.
     */
    private SharpSLCRajawaliRenderer setupGLViewAndRenderer(){
        SharpSLCRajawaliRenderer renderer = new SharpSLCRajawaliRenderer(this);
        glView = (RajawaliSurfaceView) findViewById(R.id.gl_surface_view);
        //glView.setEGLContextClientVersion(2);
        //glView.setSurfaceRenderer(renderer);
        return renderer;
    }

    /**
     * Sets up TangoUX layout and sets its listener.
     */
    private TangoUx setupTangoUxAndLayout(){
        TangoUxLayout uxLayout = (TangoUxLayout) findViewById(R.id.layout_tango);
        TangoUx tangoUx = new TangoUx(this);
        tangoUx.setLayout(uxLayout);
        tangoUx.setUxExceptionEventListener(mUxExceptionListener);
        return  tangoUx;
    }

    /*
    private float getAveragedDepth(FloatBuffer pointCloudBuffer){
        int pointCount = pointCloudBuffer.capacity() / 3;
        float totalZ = 0;
        float averageZ = 0;
        for (int i = 0; i < pointCloudBuffer.capacity() - 3; i = i + 3) {
            totalZ = totalZ + pointCloudBuffer.get(i + 2);
        }
        if (pointCount != 0)
            averageZ = totalZ / pointCount;
        return  averageZ;
    }
    */
    /**
     * Calculates the average depth from a point cloud buffer.
     *
     * @param pointCloudBuffer
     * @param numPoints
     * @return Average depth.
     */
    private float getAveragedDepth(FloatBuffer pointCloudBuffer, int numPoints) {
        float totalZ = 0;
        float averageZ = 0;
        if (numPoints != 0) {
            int numFloats = 4 * numPoints;
            for (int i = 2; i < numFloats; i = i + 4) {
                totalZ = totalZ + pointCloudBuffer.get(i);
            }
            averageZ = totalZ / numPoints;
        }
        return averageZ;
    }
    /**
     * Use the TangoSupport library with point cloud data to calculate the plane
     * of the world feature pointed at the location the camera is looking.
     * It returns the pose of the fitted plane in a TangoPoseData structure.
     */

    private float[] doFitPlane(float u, float v, double rgbTimestamp) {
        TangoPointCloudData pointCloud = mPointCloudManager.getLatestPointCloud();

        if (pointCloud == null) {
            return null;
        }

        // Get pose transforms for depth/color cameras from world frame in
        // OpenGL engine space.
        TangoPoseData openglToDepthPose = TangoSupport.getPoseAtTime(
                pointCloud.timestamp,
                TangoPoseData.COORDINATE_FRAME_AREA_DESCRIPTION,
                TangoPoseData.COORDINATE_FRAME_CAMERA_DEPTH,
                TangoSupport.TANGO_SUPPORT_ENGINE_OPENGL,
                TangoSupport.TANGO_SUPPORT_ENGINE_TANGO,
                TangoSupport.ROTATION_IGNORED);
        if (openglToDepthPose.statusCode != TangoPoseData.POSE_VALID) {
            Log.d(TAG, "Could not get a valid pose from area description "
                    + "to depth camera at time " + pointCloud.timestamp);

            // Disable any live robotic control after some count
            //if (ROBOT_CONTROL_LIVE)
            //    toggleRobotControl();
        }

        TangoPoseData openglToColorPose = TangoSupport.getPoseAtTime(
                rgbTimestamp,
                TangoPoseData.COORDINATE_FRAME_AREA_DESCRIPTION,
                TangoPoseData.COORDINATE_FRAME_CAMERA_COLOR,
                TangoSupport.TANGO_SUPPORT_ENGINE_OPENGL,
                TangoSupport.TANGO_SUPPORT_ENGINE_TANGO,
                TangoSupport.ROTATION_IGNORED);
        if (openglToDepthPose.statusCode != TangoPoseData.POSE_VALID) {
            Log.d(TAG, "Could not get a valid pose from area description "
                    + "to color camera at time " + rgbTimestamp);

            // Disable any live robotic control
            //if (ROBOT_CONTROL_LIVE)
            //    toggleRobotControl();
            //return null;
        }

        // Plane model is in OpenGL space due to input poses.
        IntersectionPointPlaneModelPair intersectionPointPlaneModelPair =
                TangoSupport.fitPlaneModelNearPoint(pointCloud,
                        openglToDepthPose.translation,
                        openglToDepthPose.rotation, u, v,
                        mDisplayRotation,
                        openglToColorPose.translation,
                        openglToColorPose.rotation);


        // Set the current plane equation
        mCurrentPlane = intersectionPointPlaneModelPair.planeModel;
        // Convert plane model into 4x4 matrix. The first 3 elements of
        // the plane model make up the normal vector.
        float[] openglUp = new float[]{0, 1, 0, 0};
        float[] openglToPlaneMatrix = matrixFromPointNormalUp(
                intersectionPointPlaneModelPair.intersectionPoint,
                intersectionPointPlaneModelPair.planeModel,
                openglUp);
        return openglToPlaneMatrix;
    }
    /**
     * Calculates a transformation matrix based on a point, a normal and the up gravity vector.
     * The coordinate frame of the target transformation will be a right handed system with Z+ in
     * the direction of the normal and Y+ up.
     */
    private float[] matrixFromPointNormalUp(double[] point, double[] normal, float[] up) {
        float[] zAxis = new float[]{(float) normal[0], (float) normal[1], (float) normal[2]};
        normalize(zAxis);
        float[] xAxis = crossProduct(up, zAxis);
        normalize(xAxis);
        float[] yAxis = crossProduct(zAxis, xAxis);
        normalize(yAxis);
        float[] m = new float[16];
        Matrix.setIdentityM(m, 0);
        m[0] = xAxis[0];
        m[1] = xAxis[1];
        m[2] = xAxis[2];
        m[4] = yAxis[0];
        m[5] = yAxis[1];
        m[6] = yAxis[2];
        m[8] = zAxis[0];
        m[9] = zAxis[1];
        m[10] = zAxis[2];
        m[12] = (float) point[0];
        m[13] = (float) point[1];
        m[14] = (float) point[2];
        return m;
    }

    /**
     * Normalize a vector.
     */
    private void normalize(float[] v) {
        double norm = Math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
        v[0] /= norm;
        v[1] /= norm;
        v[2] /= norm;
    }

    /**
     * Cross product between two vectors following the right-hand rule.
     */
    private float[] crossProduct(float[] v1, float[] v2) {
        float[] result = new float[3];
        result[0] = v1[1] * v2[2] - v2[1] * v1[2];
        result[1] = v1[2] * v2[0] - v2[2] * v1[0];
        result[2] = v1[0] * v2[1] - v2[0] * v1[1];
        return result;
    }
    /**
     * Compute the depth camera to display's rotation. This is used for rendering
     * camera in the correct rotation.
     */
    private void setAndroidOrientation() {
        Display display = getWindowManager().getDefaultDisplay();
        mDisplayRotation = display.getRotation();
        /*
        Camera.CameraInfo depthCameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(1, depthCameraInfo);

        int depthCameraRotation = Surface.ROTATION_0;
        switch(depthCameraInfo.orientation) {
            case 90:
                depthCameraRotation = Surface.ROTATION_90;
                break;
            case 180:
                depthCameraRotation = Surface.ROTATION_180;
                break;
            case 270:
                depthCameraRotation = Surface.ROTATION_270;
                break;
        }

        mDepthCameraToDisplayRotation = display.getRotation() - depthCameraRotation;
        if (mDepthCameraToDisplayRotation < 0) {
            mDepthCameraToDisplayRotation += 4;
        }
        */
    }
}
