/*
 * Copyright 2014 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.joshfinken.sharpslc;

import com.google.atap.tangoservice.TangoPointCloudData;
import com.google.atap.tangoservice.TangoPoseData;

import android.content.Context;
import android.graphics.Color;
import android.view.MotionEvent;

import org.rajawali3d.math.Matrix4;
import org.rajawali3d.math.Quaternion;
import org.rajawali3d.math.vector.Vector3;
import org.rajawali3d.renderer.RajawaliRenderer;
import org.rajawali3d.materials.Material;

import com.joshfinken.sharpslc.rajawali.PointCloud;
import com.joshfinken.sharpslc.rajawali.FrustumAxes;
import com.joshfinken.sharpslc.rajawali.Grid;
import com.joshfinken.sharpslc.rajawali.DeviceExtrinsics;
import com.joshfinken.sharpslc.rajawali.ScenePoseCalculator;
import com.joshfinken.sharpslc.rajawali.Pose;

import java.util.ArrayList;

import org.rajawali3d.primitives.Plane;
/**
 * Renderer for Point Cloud data.
 */
public class SharpSLCRajawaliRenderer extends RajawaliRenderer {

    private static final float CAMERA_NEAR = 0.01f;
    private static final float CAMERA_FAR = 200f;
    private static final int MAX_NUMBER_OF_POINTS = 60000;
    private static final float PLANE_SIDE_LENGTH = 0.5f;

    private TouchViewHandler mTouchViewHandler;

    // Objects rendered in the scene.
    private PointCloud mPointCloud;
    private FrustumAxes mFrustumAxes;
    private Grid mGrid;
    private DeviceExtrinsics mDeviceExtrinsics;

    private ArrayList<Plane> mPlanes;
    private int planeNum=0;
    private int MAX_PLANES=1500;    // Max number of planes
    private Pose mObjectPose;

    private org.rajawali3d.math.Plane mGroundPlane;
    private org.rajawali3d.math.Plane mVertPlane;

    public SharpSLCRajawaliRenderer(Context context) {
        super(context);
        mTouchViewHandler = new TouchViewHandler(mContext, getCurrentCamera());
    }

    @Override
    protected void initScene() {
        mGrid = new Grid(100, 1, 1, 0xFFCCCCCC);
        mGrid.setPosition(0, -1.3f, 0);
        getCurrentScene().addChild(mGrid);

        mFrustumAxes = new FrustumAxes(3);
        getCurrentScene().addChild(mFrustumAxes);

        // Indicate 4 floats per point since the point cloud data comes
        // in XYZC format.
        mPointCloud = new PointCloud(MAX_NUMBER_OF_POINTS, 4);
        getCurrentScene().addChild(mPointCloud);
        getCurrentScene().setBackgroundColor(Color.BLACK);
        getCurrentCamera().setNearPlane(CAMERA_NEAR);
        getCurrentCamera().setFarPlane(CAMERA_FAR);
        getCurrentCamera().setFieldOfView(37.5);


        //---------------------------------------------------------------------
        // Some issue with rajawali: objects added after init-scene are not
        // rendered.  Allocating placeholder geometry here
        //---------------------------------------------------------------------
        mPlanes = new ArrayList<Plane>();
        // Removing debug plane rendering...
        //for(int i=0; i<MAX_PLANES; i++) {
        //    Plane obj = createGeometry();
        //    mPlanes.add(obj);
        //    getCurrentScene().addChild(obj);
        //}
    }
    private synchronized Plane createGeometry() {
        Material material = new Material();
        material.setColor(0x7f009900); // green

        Plane object = new Plane(PLANE_SIDE_LENGTH, PLANE_SIDE_LENGTH, 1, 1, Vector3.Axis.Z, false, false);
        object.setMaterial(material);
        object.setTransparent(true);   //jf
        object.setPosition(0, 0, -3);
        //object.setRotation(Vector3.Axis.Z, 180); // should be unnecessary for planes
        return object;
    }


    /**
     * Updates the rendered point cloud. For this, we need the point cloud data and the device pose
     * at the time the cloud data was acquired.
     * NOTE: This needs to be called from the OpenGL rendering thread.
     */
    public void updatePointCloud(TangoPointCloudData pointCloudData, float[] openGlTdepth) {
        mPointCloud.updateCloud(pointCloudData.numPoints, pointCloudData.points);
        Matrix4 openGlTdepthMatrix = new Matrix4(openGlTdepth);
        mPointCloud.setPosition(openGlTdepthMatrix.getTranslation());
        // Conjugating the Quaternion is need because Rajawali uses left handed convention.
        mPointCloud.setOrientation(new Quaternion().fromMatrix(openGlTdepthMatrix).conjugate());
    }

    /**
     * Updates our information about the current device pose.
     * NOTE: This needs to be called from the OpenGL rendering thread.
     */
    public void updateCameraPose(TangoPoseData cameraPose) {
        float[] rotation = cameraPose.getRotationAsFloats();
        float[] translation = cameraPose.getTranslationAsFloats();
        Quaternion quaternion = new Quaternion(rotation[3], rotation[0], rotation[1], rotation[2]);
        mFrustumAxes.setPosition(translation[0], translation[1], translation[2]);
        // Conjugating the Quaternion is need because Rajawali uses left handed convention for
        // quaternions.
        mFrustumAxes.setOrientation(quaternion.conjugate());
        mTouchViewHandler.updateCamera(new Vector3(translation[0], translation[1], translation[2]),
                quaternion);
    }

    @Override
    public void onOffsetsChanged(float v, float v1, float v2, float v3, int i, int i1) {
    }

    @Override
    public void onTouchEvent(MotionEvent motionEvent) {
        mTouchViewHandler.onTouchEvent(motionEvent);
    }

    public void setFirstPersonView() {
        mTouchViewHandler.setFirstPersonView();
    }

    public void setTopDownView() {
        mTouchViewHandler.setTopDownView();
    }

    public void setThirdPersonView() {
        mTouchViewHandler.setThirdPersonView();
    }
    /**
     * Create new geometry and set its pose
     * NOTE: This needs to be called from the OpenGL rendering thread.
     */
    //public synchronized void addObjectPose(TangoPoseData planeFitPose, double[] planeEq) {
    public synchronized void addObjectPose(float[] planeFitTransform, double[] planeEq) {

        // Get next allocated plane
        Plane object = mPlanes.get(planeNum);
        if (object == null)
            return;

        Matrix4 objectTransform = new Matrix4(planeFitTransform);
        // Place the 3D object in the location of the detected plane.
        object.setPosition(objectTransform.getTranslation());
        // Note that Rajawali uses left-hand convention for Quaternions so we need to
        // specify a quaternion with rotation in the opposite direction.
        object.setOrientation(new Quaternion().fromMatrix(objectTransform));


        // Move it forward by half of the size of the cube to make it
        // flush with the plane surface.
        object.moveForward(PLANE_SIDE_LENGTH / 2.0f);

        // Rotate a vector in OpenGL space by the transformed quaternion and check
        // if Y dominates the orientation.  This is a ground plane.
        Vector3 v = new Vector3(Vector3.Z);
        v.rotateBy(new Quaternion().fromMatrix(objectTransform));

        if(Math.abs(v.y) >= 0.90){
            object.getMaterial().setColor(0x7f009900); // green: Y-up
            mGroundPlane = new org.rajawali3d.math.Plane();
            mGroundPlane.setComponents(planeEq[0], planeEq[1], planeEq[2], planeEq[3]);
        } else {
            //object.getMaterial().setColor(0x7fff0000); // red: horizontal
            // ARGB
            object.getMaterial().setColor(0x50a6a6a6);   // horizontal
            mVertPlane = new org.rajawali3d.math.Plane();
            mVertPlane.setComponents(planeEq[0], planeEq[1], planeEq[2], planeEq[3]);
        }

        //Log.i(TAG, "[hessian normal (vert)]  " + hessian.getNormal().toString());
        planeNum++;
        if(planeNum==MAX_PLANES)
            planeNum=0;
    }
    /**
     * Sets the extrinsics between different sensors of a Tango Device
     *
     * @param imuTDevicePose : Pose transformation between Device and IMU sensor.
     * @param imuTColorCameraPose : Pose transformation between Color camera and IMU sensor.
     * @param imuTDepthCameraPose : Pose transformation between Depth camera and IMU sensor.
     */
    public void setupExtrinsics(TangoPoseData imuTDevicePose, TangoPoseData imuTColorCameraPose,
                                TangoPoseData imuTDepthCameraPose) {
        mDeviceExtrinsics =
                new DeviceExtrinsics(imuTDevicePose, imuTColorCameraPose, imuTDepthCameraPose);
    }
    public DeviceExtrinsics getDeviceExtrinsics() {
        return mDeviceExtrinsics;
    }
}
