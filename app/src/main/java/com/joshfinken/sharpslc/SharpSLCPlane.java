package com.joshfinken.sharpslc;

/**
 * Created by jfinken on 9/22/2016.
 */
public class SharpSLCPlane {
    private final int SAMPLE_SIZE = 10;
    private float u;
    private float v;
    private String direction;
    // plane equation
    private double a;
    private double b;
    private double c;
    private double d;
    private double distToPlane;
    private double prevAvgDist;
    private SharpSLCCircularBuffer<Double> distBuffer;

    public SharpSLCPlane(String dir, float _u, float _v) {
        this.direction = dir;
        this.u = _u;
        this.v = _v;
        this.distBuffer = new SharpSLCCircularBuffer<Double>(SAMPLE_SIZE);
    }
    public void setPlaneEquation(double _a, double _b, double _c, double _d) {
        a = _a;
        b = _b;
        c = _c;
        d = _d;

        // p = d / sqrt(a^2 + b^2 + c^2))
        distToPlane = d / Math.sqrt(Math.pow(a, 2) +
                Math.pow(b, 2) +
                Math.pow(c, 2));

        distBuffer.add(distToPlane);
    }
    // TODO: need a historian or a more comprehensive analysis of "what I've done recently" to prevent
    // it from banging it's head over and over again.
    public double getPrevAvgDist() { return prevAvgDist; }
    public void setPrevAvgDist(double d) { prevAvgDist = d;}
    public double getAvgDist() {
        return distBuffer.getMovingAvg();
    }
    public String getDirection() {
        return direction;
    }
    public String getUi() {
        return String.format("Distance to %s plane: %.2f", direction, distToPlane);
    }
    public float getU() {
        return u;
    }
    public float getV() {
        return v;
    }
}
