package com.joshfinken.sharpslc;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;

/**
 * Created by jfinken on 9/23/2016.
 */
public class SharpSLCCircularBuffer<T> {

    private T[] buffer;
    private int tail;
    private int head;
    private int totalSamples;

    public SharpSLCCircularBuffer(int n) {
        totalSamples = 0;
        buffer = (T[]) new Object[n];
        tail = 0;
        head = 0;
    }

    public void add(T toAdd) {
        if (head != (tail - 1)) {
            buffer[head++] = toAdd;
        } else {
            throw new BufferOverflowException();
        }
        head = head % buffer.length;
        totalSamples++;
    }
    // from current tail
    public T get() {
        T t = null;
        int adjTail = tail > head ? tail - buffer.length : tail;
        if (adjTail < head) {
            t = (T) buffer[tail++];
            tail = tail % buffer.length;
        } else {
            throw new BufferUnderflowException();
        }
        return t;
    }
    public double getMovingAvg() {
        // asserting enough samples at the start
        if (totalSamples < buffer.length)
            return -1.0;
        double sum = 0.0;
        for (int i=0; i< buffer.length; ++i) {
            sum += (Double)buffer[i];
        }
        return sum / buffer.length;
    }
    public String toString() {
        return "SharpSLCCircularBuffer(size=" + buffer.length + ", head=" + head +
                ", tail=" + tail + ")";
    }
}
