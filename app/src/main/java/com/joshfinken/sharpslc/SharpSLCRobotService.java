package com.joshfinken.sharpslc;

import com.android.volley.toolbox.Volley;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.AuthFailureError;
import com.google.atap.tango.reconstruction.TangoPolygon;

import android.content.Context;
import android.util.Log;
import java.util.List;

import com.google.gson.Gson;

/**
 * Created by jfinken on 9/20/2016.
 *
 * Sample routes:
 *  /api/v1/tread/dir/forward/duration/5
 *  /api/v1/tread/dir/left/duration/2
 *  /api/v1/tread/dir/right/duration/2
 *  /api/v1/tread/dir/backward/duration/5
 *
 *  /api/v1/floorplan/fp1234
 */
public class SharpSLCRobotService {
    private static final String TAG = SharpSLCRobotService.class.getSimpleName();
    public static final String FORWARD = "forward";
    public static final String BACKWARD = "backward";
    public static final String LEFT = "left";
    public static final String RIGHT = "right";
    private final String IP = "http://192.168.1.3:8181";
    private final String CONTROL_URL = "/api/v1/tread/dir/%1$s/duration/%2$d";
    private final String FP_ID = "fp1234";
    private final String FLOORPLAN_URL = "/api/v1/floorplan/%1$s";
    private static SharpSLCRobotService mInstance;
    private RequestQueue mRequestQueue;
    private static Context mCtx;
    private SharpSLCRobotService(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized SharpSLCRobotService getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharpSLCRobotService(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }
    public void ControlRequest(String direction, int duration) {
        // build final url
        String route = String.format(CONTROL_URL, direction, duration);
        addToRequestQueue(makeRequest(route));
    }
    public void PutFloorplanRequest(List<TangoPolygon> polygons) {

        // JSON body
        Gson gson = new Gson();
        String json = gson.toJson(polygons);

        //Log.d(TAG, json);
        String route = String.format(FLOORPLAN_URL, FP_ID);
        StringRequest req = makePutRequest(route, json);
        addToRequestQueue(req);
    }
    private <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
    private StringRequest makeRequest(String route) {
        // String route = String.format(CONTROL_URL, direction, duration);
        String url = String.format("%1s%2s", IP, route);
        Log.i(TAG, url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // process your response here

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //perform operation here after getting error
            }
        });
        return stringRequest;
    }

    private StringRequest makePutRequest(String route, final String data) {
        // String route = String.format(CONTROL_URL, direction, duration);
        String url = String.format("%1s%2s", IP, route);
        Log.i(TAG, url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // process your response here

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //perform operation here after getting error
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                String this_json = data;
                return this_json.getBytes();
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        return stringRequest;
    }
}
